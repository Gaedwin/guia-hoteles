const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const del = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-usemin');
const rev = require('gulp-rev');
const cleanCss = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');

function style(){
    return  gulp.src('./css/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./css'))
        .pipe(browserSync.stream());
}

function watch(){
    browserSync.init({
        server: {
            baseDir:'./'
        }
    });
    gulp.watch('./css/*.scss', style);
    gulp.watch('./*.hmtl').on('change',browserSync.reload);
    gulp.watch('./js/*.js').on('change',browserSync.reload);
}

gulp.task('clean', ()=>{
    return del(['dist']);
});
gulp.task('copyFonts', ()=>{
    gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
        .pipe(gulp.dest('./dist/fonts'));
});
gulp.task('imagemin', function(){
    return gulp.src('./img/*.{png, jpg,jpeg gif}')
        .pipe(imagemin({optimizationLevel: 3, progressive: true, interlaced: true}))
        .pipe(gulp.dest('dist/img'));
})

gulp.task('usemin', () =>{
    return gulp.src('./*hmtl')
        .pipe(flatmap(function(stream, file){
            return stream 
                .pipe(usemin({
                    css: [rev()],
                    html: [function(){return htmlmin({collapseWhitespace: true})}],
                    js: [uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleanCss(), 'concat']
                }));
        }))
        .pipe(gulp.dest('dist/'));
});
function build(){
    gulp.series(['copyFonts','clean', 'imagemin', 'usemin']);
};

exports.style = style;
exports.watch = watch;

exports.build = build;