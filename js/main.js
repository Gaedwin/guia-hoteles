$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();   
});
$(function(){
    $("#contacto").on("show.bs.modal", function (e) {
        console.log("El modal se esta mostrando");

        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log("El modal se muestra");
    });
    $('#contacto').on('hide.bs.modal', function (e){
        console.log('se oculta');
        $('#contactoBtn').addClass('btn-outline-success');        
    });
    $('#contacto').on('hidden.bs.modal', function (e){
        console.log('se oculto');
        $('#contactoBtn').prop('disabled', false);        
    })
});
$(function(){
    $(".carousel").carousel({
        interval:2000
    }); 
});